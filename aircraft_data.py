from typing import List, Optional
from math import sqrt, pow
from csvs import read_data


# from drawplot import plotit


class FlightMode:
    V_TAS: float  # m/s
    fuel: float  # kg/m
    V_y: float  # m/s
    V_x: float  # m/s

    def __init__(self, V_TAS, fuel, V_y, V_x):
        self.V_TAS = V_TAS
        self.V_x = V_x
        self.V_y = V_y
        self.fuel = fuel


class Echelon:
    height: float  # 100 feet
    cruise: Optional[FlightMode] = None
    climb: Optional[FlightMode] = None
    descent: Optional[FlightMode] = None

    def __init__(self, height, cruise, climb, descent):
        self.climb = climb
        self.cruise = cruise
        self.descent = descent
        self.height = height

    def h_in_meters(self):
        return self.height * 100 * 0.3048

    def h_in_feet(self):
        return self.height * 100


class AircraftData:
    echs: list
    mass: int

    def __init__(self, filename, mass):
        self.mass = mass
        self.echs = []
        lst = read_data(filename)
        for i in range(5, len(lst)):
            if lst[i][1] != "-":
                cruise = FlightMode(float(lst[i][2]), float(lst[i][6 + mass]), 0, float(lst[i][2]))
            else:
                cruise = None
            climb = FlightMode(float(lst[i][10]), float(lst[i][21]), float(lst[i][14 + mass]), float(lst[i][17]))
            descent = FlightMode(float(lst[i][26]), float(lst[i][31]), -float(lst[i][28]), float(lst[i][29]))
            echelon = Echelon(float(lst[i][0]), cruise, climb, descent)
            self.echs.append(echelon)

    def climb_step(self, ech_num):  # transition to the next echelon
        if ech_num < (len(self.echs) - 1):
            dh = self.echs[ech_num + 1].h_in_meters() - self.echs[ech_num].h_in_meters()
            T = dh / self.echs[ech_num].climb.V_y
            X = T * self.echs[ech_num].climb.V_x
            F = self.echs[ech_num].climb.fuel * T * self.echs[ech_num].climb.V_TAS
            return T, X, F
        return 0, 0, 0

    def descent_step(self, ech_num):
        if ech_num > 0:
            dh = self.echs[ech_num].h_in_meters() - self.echs[ech_num - 1].h_in_meters()
            T = - dh / self.echs[ech_num].descent.V_y
            # print(dh)
            # print(self.echs[ech_num].descent.V_x)
            X = T * self.echs[ech_num].descent.V_x
            # print(T, X)
            F = self.echs[ech_num].climb.fuel * T * self.echs[ech_num].climb.V_TAS
            return T, X, F
        return 0, 0, 0

    def calc_climb(self, ech_num):  # climb to ech_num echelon
        climb_pts = []
        T = 0  # climb time
        S = 0  # horizontal distance travelled during the climb
        F = 0  # fuel spent
        for i in range(ech_num):
            climb_pts.append([S, self.echs[i].h_in_meters(), T])
            if i < ech_num - 1:
                dT, dX, dF = self.climb_step(i)
                # print(dT, dX)
                T += dT
                S += dX
                F += dF
        return T, S, F, climb_pts

    def calc_cruise(self, echelon, S, numpts):
        T = 0
        dT = (S / echelon.cruise.V_TAS) / numpts
        cruise_pts = []
        for i in range(numpts):
            cruise_pts.append([S / numpts * i, echelon.h_in_meters(), T])
            T += dT
        F = echelon.cruise.fuel * echelon.cruise.V_TAS
        return T, F, cruise_pts

    def calc_descent(self, ech_num):
        T = 0  # descent time
        S = 0  # horizontal distance travelled during the descent
        F = 0  # fuel spent
        descent_pts = []
        for i in reversed(range(ech_num)):
            descent_pts.append([S, self.echs[i].h_in_meters(), T])
            # if i > 0:
            dT, dX, dF = self.descent_step(i)
            # print(dT, dX)
            T += dT
            S += dX
            F += dF
        return T, S, F, descent_pts

    def find_target_echelon(self, h):
        error = self.echs[-1].h_in_meters()
        i_target = 0
        for i in range(len(self.echs)):
            if abs(self.echs[i].h_in_meters() - h) < abs(error):
                i_target = i
                error = self.echs[i].h_in_meters() - h
        return i_target, self.echs[i_target].h_in_meters(), error




