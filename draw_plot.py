from csvs import read_track
import matplotlib
from tracks import *
# from tkinter import *
from collections.abc import Iterable
import matplotlib.pyplot as plt
from draft import *


matplotlib.use('TkAgg')


def real_track(name):
    T, dist, track = read_track(name)

    # print(track)
    trackX, trackY = [], []
    for [x, y] in track:
        trackX.append(float(x))
        trackY.append(float(y) / 100)
    return T, dist, trackX, trackY


def plot_echelones(ad):
    for ech in ad.echs:
        h = ech.h_in_meters()
        plt.axhline(y=h, c='grey', lw=0.5, alpha=0.5)
        # ax.plot(x, h,
        #         linestyle='-',
        #         linewidth=0.5,
        #         color='grey'
        #         )


# def plot_it(track, real_track_name, ad):
#     _, ax = plt.subplots()
#     trackX, trackY = [], []
#     for [x, y] in track:
#         trackX.append(x / 1000)
#         trackY.append(y * 3.28 / 100)
#     ax.plot(trackX, trackY,
#             linestyle='--',
#             linewidth=2,
#             color='darkblue'
#             )
#     T, dist, tX, tY = real_track(real_track_name)
#     ax.plot(tX, tY,
#             linestyle='-',
#             linewidth=1,
#             color='crimson'
#             )
#     plot_echelones(ad)
#     plt.show()


def plot_obj(obj, ax, color):
    if ax == 0:
        _, ax = plt.subplots()
    if isinstance(obj, Track):
        objX, objY = obj.unpack("X")
    else:
        if isinstance(obj, FlightPlan):
            objX, objY = obj.unpack("X")
        else:
            objX, objY = map(list, zip(*obj))
    ax.plot(objX, objY,
            linestyle='-',
            linewidth=1,
            color=color
            )
    # if not isinstance(obj, Track):
    #     plt.show()


def plot_scatter(obj, ax):
    if ax == 0:
        _, ax = plt.subplots()
    if isinstance(obj, Chain):
        objX, objY = obj.unpack("X")
    else:
        if isinstance(obj, FlightPlan):
            objX, objY = obj.unpack("X")
        else:
            objX, objY = map(list, zip(*obj))
    ax.scatter(objX, objY,
               marker='o',
               color='darkblue',
               edgecolor='black',
               alpha=0.5,
               s=50
               )
    # if not isinstance(obj, Chain):
    #     plt.show()


def plot_multiple(objs_plot, objs_scatter, ad):
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'b']
    _, ax = plt.subplots()
    for i, obj in enumerate(objs_plot):
        plot_obj(obj, ax, colors[i])
    for obj in objs_scatter:
        plot_scatter(obj, ax)

    plot_echelones(ad)
    plt.show()
