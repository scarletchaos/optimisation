from calculations import *


class Point:
    H: float  # Height, m
    X: float  # Length of ground-projected trajectory
    T: float  # Flight time till this point

    def __init__(self, h, x, t):
        self.H = h
        self.X = x
        self.T = t


class Track(list):
    def simplified(self, error):
        s_track = self.copy()
        err_min = 0
        while err_min < error:
            i_min = 0
            err_min = error
            for i in range(1, len(s_track) - 1):
                if err_from_abc(s_track[i - 1], s_track[i], s_track[i + 1]) < err_min:
                    i_min = i
                    err_min = err_from_abc(s_track[i - 1], s_track[i], s_track[i + 1])
            if i_min != 0:
                del s_track[i_min]
        return Track(s_track)

    def unpack(self, unpack_type):
        objX, objY = [], []
        if unpack_type == "T":
            for [_, y, x] in self:
                objX.append(float(x))
                objY.append(float(y))
        else:
            for [x, y, _] in self:
                objX.append(float(x))
                objY.append(float(y))
        return objX, objY


def make_track(S_total, n_pts, ad, ech_num):
    T_climb, S_climb, F_climb, Traj_climb = ad.calc_climb(ech_num + 1)
    T_descent, S_descent, F_descent, Traj_descent = ad.calc_descent(ech_num + 1)
    S_cruise = S_total - S_descent - S_climb
    # print(S_cruise)
    T_cruise, F_cruise, Traj_cruise = ad.calc_cruise(ad.echs[ech_num], S_cruise, n_pts)
    for i in range(len(Traj_cruise)):
        Traj_cruise[i][0] += S_climb
        Traj_cruise[i][2] += T_climb
        # print(x)
    for i in range(len(Traj_descent)):
        Traj_descent[i][0] += S_climb + S_cruise
        Traj_descent[i][2] += T_climb + T_cruise
    return T_climb + T_cruise + T_descent, F_climb + F_cruise + F_descent, Traj_climb + Traj_cruise + Traj_descent


class Node:
    H: float  # echelon
    S: float  # Path len from 0
    H_target: float = 0  # target echelon
    T: float
    delta_X: float = 0  # dX
    length: float = 0  # till next node

    def __init__(self, h, s, t):
        self.H = h  # ad.find_target_echelon(h)
        self.S = s
        self.T = t


class Chain(list):
    def __init__(self, track):
        super().__init__()
        for i in range(len(track)):
            self.append(Node(track[i][1], track[i][0], track[i][2]))

    def unpack(self, unpack_type):
        objX, objY = [], []
        for i in range(len(self)):
            if unpack_type == "T":
                objX.append(self[i].T)
            else:
                objX.append(self[i].S)
            objY.append(self[i].H)
        return objX, objY

    def clean_low_echelones(self, start=0, fin=3000):  # deletes points in (start, fin) from Chain object
        to_delete = []
        for i in range(len(self)):
            if fin > self[i].H > start:
                to_delete.append(i)
                # print(self[i].H)
                # print(i)
        for i in reversed((range(len(to_delete)))):
            del self[to_delete[i]]

    def new_node(self, i, alpha, sign):
        return Node(self[i].H * alpha + self[i + sign].H * (1 - alpha),
                    self[i].S * alpha + self[i + sign].S * (1 - alpha),
                    self[i].T * alpha + self[i + sign].T * (1 - alpha))

    def move_node(self, i, new_node):
        self[i] = new_node

    def check_for_timings(self, sec):
        flag = True
        while flag:
            flag = False
            for i in range(len(self) - 1):
                dT = self[i + 1].T - self[i].T
                if dT > (sec * 1.5):
                    alpha = sec / dT
                    self.insert(i + 1, self.new_node(i, alpha, 1))
                    flag = True

    def check_for_echelones(self, ad, error):
        for i in reversed(range(len(self))):
            _, _, err = ad.find_target_echelon(self[i].H)
            if abs(err) > error:
                del self[i]

    def snap_to_echelones(self, ad):
        for i in range(len(self)):
            ech_num, ech_h, err = ad.find_target_echelon(self[i].H)
            flag = False
            if abs(err) > 1 and not flag:
                sign = int(err / abs(err))
                dH = self[i + sign].H - self[i].H
                alpha = (err / dH)
                print(alpha, dH, err, sign)
                self.move_node(i, self.new_node(i, (1-alpha), sign))
