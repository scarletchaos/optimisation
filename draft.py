from calculations import *
from aircraft_data import *
from tracks import *
import random
import copy

class Action:
    action: int  # 1 - up, 0 - cruise, -1 down
    action_time: float

    def __init__(self, action, time=0):
        self.action = action
        self.action_time = time

    def get_height(self, fp, index):
        # for i in (0..index)
        return 0

    def get_velocity(self, fp, index):
        # self.height [ if self.action == 0 ] echelon cruise velocity
        # self.height [ if self.action == 1 ] echelon raise velocity
        return 0

    def get_absolute_time(self, fp, index):
        # for i in (0..index)
        # sum of all previous actions time
        return 0


class FlightPlan(list):
    total_length: float
    aircraft: AircraftData

    def __init__(self, total_length, aircraft_):
        super().__init__()
        self.total_length = total_length
        self.aircraft = aircraft_
        for i in range(len(self.aircraft.echs) - 1):
            self.append(Action(1))
        self.append(Action(0, 1))  # 0 times
        for i in range(len(self.aircraft.echs) - 1):
            self.append(Action(-1))  # 25 times
        self.patch_the_plan()

    def unpack(self, unpack_type):
        objX, objY = [], []
        for i in range(len(self)):
            if unpack_type == "T":
                objX.append(self.get_total_time(i))
            else:
                objX.append(self.get_total_length(i))
            objY.append(self.aircraft.echs[self.get_echelon(i)].h_in_meters())
        return objX, objY

    def build_track(self):
        return Track([])  # make_track

    def get_echelon(self, index):
        ech_num = 0
        for i in range(index):
            ech_num += self[i].action
            # if self[i].action == 0:
            #     if self[i-1].action == 1:
            #         ech_num -= 1
        return ech_num

    def get_velocity(self, index):
        ech_num = self.get_echelon(index)
        if self[index].action == -1:
            return self.aircraft.echs[ech_num].descent.V_TAS
        if self[index].action == 0:
            return self.aircraft.echs[ech_num].cruise.V_TAS
        if self[index].action == 1:
            return self.aircraft.echs[ech_num].climb.V_TAS
        return 0

    def get_length(self, index):
        return self.get_velocity(index) * self[index].action_time

    def get_total_length(self, index):
        total_len = 0
        for i in range(index):
            total_len += self.get_length(i)
        return total_len

    def calc_action_time(self, index):
        ech_num = self.get_echelon(index)
        action_time = 0
        if self[index].action == -1:
            action_time, _, _ = self.aircraft.descent_step(ech_num)
        if self[index].action == 1:
            action_time, _, _ = self.aircraft.climb_step(ech_num)
        return action_time

    def get_total_time(self, index):
        total_time = 0
        if index < 0:
            total_time = 0
        else:
            for i in range(index-2):
                total_time += self[i].action_time
        return total_time

    def patch_the_plan(self):
        longest_cruise = -1
        longest_cruise_index = -1
        for i in range(len(self)):  # step 1) recalculate action_time for non-cruise actions
            self[i].action_time = self.calc_action_time(i)
            if self[i].action == 0 and self[i].action_time >= longest_cruise:  # step 2) find longest cruise action
                longest_cruise = self[i].action_time
                longest_cruise_index = i
        self[longest_cruise_index].action_time = (self.total_length - self.get_length(len(self)-1)) / self.get_velocity(longest_cruise_index)
        return 0
        # step 3) change action time (considering action velocity) to fit self.total_length
        # ... self.total_length

    def remove_top_echelon(self):
        first, last = 0, 0
        for i in range(1, len(self)-1):
            if self[i].action == 1 and not self[i + 1].action == 1:
                last = i
            if self[i].action == -1 and not self[i - 1].action == -1 and first == 0:
                first = i
        del self[first]
        del self[last]
        return 0

    def find_cruise(self, time_slot):
        cruises = []
        for i in range(len(self)):
            a_time = self[i].action_time
            if self[i].action == 0 and a_time > time_slot:
                cruises.append(i)
        return cruises

    def calc_fuel(self):
        fuel = 0
        for i in range(len(self)):
            if self[i].action == -1:
                fuel += self[i].action_time * self.aircraft.echs[self.get_echelon(i)].descent.V_TAS * \
                        self.aircraft.echs[self.get_echelon(i)].descent.fuel
            if self[i].action == 0:
                fuel += self[i].action_time * self.aircraft.echs[self.get_echelon(i)].cruise.V_TAS * \
                        self.aircraft.echs[self.get_echelon(i)].cruise.fuel
            if self[i].action == 1:
                fuel += self[i].action_time * self.aircraft.echs[self.get_echelon(i)].climb.V_TAS * \
                        self.aircraft.echs[self.get_echelon(i)].climb.fuel
        return fuel

    def shuffle_cruise_time(self, time_slot):
        cp = copy.deepcopy(self)   # step 0) Create self copy
        time_slot = random.choice(range(300, 600))
        cruises = cp.find_cruise(time_slot)
        if not cruises:
            cp.remove_top_echelon()
        if not cruises:
            return cp

        cruise_num = random.choice(cruises)    # step 1) find RANDOM cruise action longer than time_slot
        cp[cruise_num].action_time -= time_slot  # step 2) Decrease cruise action time by time_slot value
        # while self.aircraft.echs[self.get_echelon(any_num)].cruise is None:
        any_num = random.choice(range(6, len(cp)-6)) # step 3) Find RANDOM any action but first.
        print(any_num)
        action = Action(0, time_slot)
        cp.insert(any_num, action)  # step 4) Insert action(0,time_slot) BEFORE
        # cp.patch_the_plan()
        return cp

    def opt(self, time_slot, i):
        cp = self.__copy__()    # step 0) Create self copy
        cruises = cp.find_cruise(time_slot)
        print(cruises)
        cruise_num = random.choice(cruises)    # step 1) find RANDOM cruise action longer than time_slot
        cp[cruise_num].action_time -= time_slot  # step 2) Decrease cruise action time by time_slot value
        # while self.aircraft.echs[self.get_echelon(any_num)].cruise is None: # step 3) Find RANDOM any action but first.
        cp.insert(i, Action(0, time_slot))  # step 4) Insert action(0,time_slot) BEFORE
        # cp.patch_the_plan()
        return cp


def track_metric(track):
    return 0

# target_length = 10000
# fp = FlightPlan(target_length)
# min_metric = 9999999999999999999999999999999
#
# for x in range(0, 10):
#     tr = fp.build_track()
#     metric = track_metric(tr)
#     print('metric: ', metric)
#     show_track(tr)
#     min_metric = min([metric, min_metric])
#
#     for _ in range(0, 10):
#         # test
#         test_fp = fp.shuffle_cruise_time(10)
#         test_tr = test_fp.build_track()
#         # Animate show_track(tr)
#
#         metric = track_metric(test_tr)
#         if metric < min_metric:
#             fp = test_fp
#             print('try success: ', metric)
#             break
#         else:
#             print('try failed: ', metric)
