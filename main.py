from tracks import *
from optimisation import *
from aircraft_data import *
from draw_plot import *
from csvs import *
import os


def compare_tracks(dir_name, ad):
    folder = []
    tracks = []
    times = []
    for i in os.walk(dir_name):
        folder.append(i)
    for address, dirs, files in folder:
        for file in files:
            tracks.append(address + '/' + file)
    writer = csv.writer(open('log.csv', 'w'))
    i = 1
    for track in tracks:
        T_real, S_real, _ = read_track(track)
        T_mod, Fuel_mod, Track_mod = make_track(S_real, 10, ad, 23)
        times.append([i, (abs(T_mod - T_real)) / 60])
        result = [track, abs(T_mod - T_real), Fuel_mod]
        write_log(writer, result)
        i += 1
    plot_scatter(times, 0)


b737 = AircraftData("dataset/737.csv", 2)
T_real, S_real, track0 = read_track("dataset/B737-700/DP537_26c05ff1.csv")

total_len = S_real
fp = FlightPlan(total_len, b737)
fp.patch_the_plan()
cp = []
opt = -1

for i in range(1):  # number of steps, 0 = non-modified track
    if opt < 0:
        cp.append(fp.shuffle_cruise_time(600))
    else:
        cp.append(cp[opt].shuffle_cruise_time(600))
    if opt < 0:
        if cp[i].calc_fuel() < fp.calc_fuel():
            opt = i
    else:
        if cp[i].calc_fuel() < cp[opt].calc_fuel():
            opt = i
    a = 0

print(S_real, fp.get_total_length(len(fp)))
print(fp.calc_fuel())
print(cp[opt].calc_fuel())
plot_multiple([cp[opt]], [cp[opt]], b737)
