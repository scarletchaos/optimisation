from math import sin, cos, sqrt, atan2, radians
import os


def err_from_abc(a, b, c):
    # print(a, b, c)
    ab = ((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2) ** 0.5
    bc = ((c[0] - b[0]) ** 2 + (c[1] - b[1]) ** 2) ** 0.5
    ac = ((a[0] - c[0]) ** 2 + (a[1] - c[1]) ** 2) ** 0.5
    # print(ab, bc, ac)
    p = (ab + bc + ac) / 2
    # print(p)
    S = 2 * (p * (p - ab) * (p - bc) * (p - ac)) ** 0.5
    # print(S)
    h = S / ac
    return S / 100000


def feet_to_meters(feet):
    return feet * 0.3048


def calculate_distance(start, end):
    R = 6373.0
    startLat, startLon = start.split(',')
    endLat, endLon = end.split(',')
    lat1, lon1, lat2, lon2 = radians(float(startLat)), radians(float(startLon)), radians(float(endLat)), radians(
        float(endLon))
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c


def dist_with_h(dx, dh):
    return (dx ** 2 + dh ** 2) ** 0.5
