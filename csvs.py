import csv

from calculations import *


def write_log(writer, result):
    writer.writerow(result)


def UTC_to_sec(lst):
    T_str = ((lst[1]).split("T")[1]).split("Z")[0]
    T_lst = T_str.split(":")
    T = int(T_lst[0]) * 3600 + int(T_lst[1]) * 60 + int(T_lst[2])
    return T


def read_track(filename):
    # print(filename)
    reader = csv.reader(open(filename, 'r'))
    lst = list(reader)
    dist = 0  # distance
    track = list([])
    track.append([0, 0, 0])
    i_1 = 0
    i_2 = 0
    T = 0
    for i in range(3, len(lst) - 1):
        if float(lst[i + 1][4]) > 0 and float(lst[i][4]) == 0:
            i_1 = i
        if float(lst[i - 1][4]) > 0 and float(lst[i][4]) == 0:
            i_2 = i
        if float(lst[i + 1][4]) > 0 or float(lst[i - 1][4]) > 0:
            dist += dist_with_h(calculate_distance(lst[i][3], lst[i - 1][3]), abs(float(lst[i + 1][4]) - float(lst[i + 1][4])))
            t = UTC_to_sec(lst[i]) - UTC_to_sec(lst[i_1])
            track.append([dist*1000, float(lst[i][4]) * 0.3048, t])
    dist *= 1000
    T_1 = UTC_to_sec(lst[i_1])
    T_2 = UTC_to_sec(lst[i_2])
    T = T_2 - T_1  # travel time
    return T, dist, track


def read_data(filename):
    reader = csv.reader(open(filename, 'r'))
    return list(reader)

# print(read_track("dataset/B737-700/DP537_26ad1f6f.csv"))
